var searchData=
[
  ['cal_5fhist_1',['cal_hist',['../clahe_g_o_8cpp.html#a43141ef396bfd67054312634d64ab1e0',1,'cal_hist(cv::Mat src, int start_x, int end_x, int start_y, int end_y):&#160;claheGO.cpp'],['../clahe_g_o_8hpp.html#a43141ef396bfd67054312634d64ab1e0',1,'cal_hist(cv::Mat src, int start_x, int end_x, int start_y, int end_y):&#160;claheGO.cpp']]],
  ['cd_5fhist_2',['cd_hist',['../clahe_g_o_8cpp.html#a6e04bf2d079202027e4d8b7524c9cf45',1,'cd_hist(int *tmp2, int total):&#160;claheGO.cpp'],['../clahe_g_o_8hpp.html#a6e04bf2d079202027e4d8b7524c9cf45',1,'cd_hist(int *tmp2, int total):&#160;claheGO.cpp']]],
  ['clahe_2ecpp_3',['clahe.cpp',['../clahe_8cpp.html',1,'']]],
  ['clahego_4',['claheGO',['../clahe_g_o_8cpp.html#a14ed066403aa4246c265d75039fdcc60',1,'claheGO(cv::Mat src, int step=8):&#160;claheGO.cpp'],['../clahe_g_o_8hpp.html#a30a2e11bc86807fc4028aaa93afd7da8',1,'claheGO(cv::Mat src, int step):&#160;claheGO.cpp']]],
  ['clahego_2ecpp_5',['claheGO.cpp',['../clahe_g_o_8cpp.html',1,'']]],
  ['clahego_2ehpp_6',['claheGO.hpp',['../clahe_g_o_8hpp.html',1,'']]],
  ['ctpv_7',['ctpv',['../clahe_g_o_8cpp.html#a7090c30087faffd7a68d8901a3277f53',1,'ctpv(cv::Mat src, int block, int width, int height, int width_block, int height_block, float **C2):&#160;claheGO.cpp'],['../clahe_g_o_8hpp.html#a7090c30087faffd7a68d8901a3277f53',1,'ctpv(cv::Mat src, int block, int width, int height, int width_block, int height_block, float **C2):&#160;claheGO.cpp']]]
];
