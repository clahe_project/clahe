#include <gtest/gtest.h>
#include <claheGO.hpp>
#include <opencv2/highgui.hpp>
#include <time.h>
#include <iostream>
#include <string>

/////////////////// GOLDEN MODELS ////////////////////
int *cal_hist_test (cv::Mat src, int start_x, int end_x, int start_y, int end_y) {
  int *tmp2;

  tmp2 = new int [256];
  for (int i = 0; i < 256; i++)
      tmp2[i] = 0;

  for (int i = start_x; i < end_x; i++) {
    for (int j = start_y; j < end_y; j++) {
      int index = src.at<uchar>(j,i);
      tmp2[index]++;
    }
  }

  return reinterpret_cast<int *>(tmp2);
}

std::tuple<int *, int> steal_test (int *tmp2, int width_block, int height_block) {
  // How to choose the parameters, you need to discuss. Discuss different results
  // About the global time, how to calculate this cl here, need to discuss 
  int average = width_block * height_block / 255;
  int LIMIT = 40 * average;
  int steal = 0;

  for (int k = 0; k < 256; k++) {
    if (tmp2[k] > LIMIT) {
      steal  += tmp2[k] - LIMIT;
      tmp2[k] = LIMIT;
    }
  }

  return std::tuple<int *, int>{tmp2, steal};
}

float *cd_hist_test (int *tmp2, int total) {
  float *C2;
  
  C2 = new float [256];
  
  for (int k = 0; k < 256; k++) {
    if (k == 0)
      C2[k] = 1.0f * tmp2[k] / total;
    else
      C2[k] = C2[k-1] + 1.0f * tmp2[k] / total;
  }
  
  return reinterpret_cast<float *>(C2);
}

TEST (cal_hist_func, test1) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 93;
  int start_y = 0;
  int end_y   = 70;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);
  
  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

float **block_f_test (cv::Mat src, int block, int width_block, int height_block) {
  float **C2;
  int *tmp2;
  int total = width_block * height_block;

  C2 = new float *[8*8];
  for (int i = 0; i < 8*8; i++)
    C2[i] = new float[256];

  for (int i = 0; i < block; i++) {
    for (int j = 0; j < block; j++) {
      int start_x = i * width_block;
      int end_x   = start_x + width_block;
      int start_y = j * height_block;
      int end_y   = start_y + height_block;
      int num     = i + block * j;

      // Traverse the small block, calculate the histogram
      tmp2 = cal_hist_test (src, start_x, end_x, start_y, end_y);

      // Crop and increase operations, that is, the cl part of the clahe
      // The parameters here correspond to "Gem" above fCliplimit = 4 , uiNrBins = 255
      auto data = steal_test (tmp2, width_block, height_block);
      tmp2      = std::get<0>(data);
      int steal = std::get<1>(data);

      int bonus = steal / 256;

      // hand out the steals averagely
      for (int k = 0; k < 256; k++) {
        tmp2[k] += bonus;
      }

      // Calculate the cumulative distribution histogram
      C2[num] = cd_hist_test (tmp2, total);
    }
  }

  return reinterpret_cast<float **>(C2);
}

cv::Mat ctpv_test (cv::Mat src, int block, int width, int height, int width_block, int height_block, float **C2) {
  cv::Mat CLAHE_GO = src.clone();

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // four coners
      if (i <= width_block / 2 && j <= height_block / 2) {
        int num = 0;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i <= width_block / 2 && j >= ((block - 1) * height_block + height_block / 2)) {
        int num = block * (block - 1);
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2) && j <= height_block / 2) {
        int num = block - 1;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2) && j >= ((block - 1) * height_block + height_block / 2)) {
        int num = block * block - 1;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      }
      // four edges except coners
      else if (i <= width_block / 2) {
        //linear interpolation
        int num_i = 0;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2)) {
        // linear interpolation
        int num_i = block - 1;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j <= height_block / 2) {
        // linear interpolation
        int num_i = (i - width_block / 2) / width_block;
        int num_j = 0;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j >= ((block - 1) * height_block + height_block / 2)) {
        // linear interpolation
        int num_i = (i - width_block / 2) / width_block;
        int num_j = block - 1;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      }
      // four edges except coners
      else if (i <= width_block / 2) {
        //linear interpolation
        int num_i = 0;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2)) {
        // linear interpolation
        int num_i = block - 1;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j <= height_block / 2) {
        // linear interpolation
        int num_i = (i - width_block / 2) / width_block;
        int num_j = 0;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j >= ((block - 1) * height_block + height_block / 2)) {
        // linear interpolation
        int num_i = (i - width_block / 2) / width_block;
        int num_j = block - 1;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      }
      // bilinear interpolation
      else {
        int num_i = (i - width_block / 2) / width_block;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        int num3  = num1 + block;
        int num4  = num2 + block;
        float u   = (i - (num_i * width_block  + width_block  / 2)) / (1.0f * width_block);
        float v   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        CLAHE_GO.at<uchar>(j,i) = (int)((   u  *    v  * C2[num4][CLAHE_GO.at<uchar>(j,i)] +
                                         (1-u) * (1-v) * C2[num1][CLAHE_GO.at<uchar>(j,i)] +
                                            u  * (1-v) * C2[num2][CLAHE_GO.at<uchar>(j,i)] +
                                         (1-u) *    v  * C2[num3][CLAHE_GO.at<uchar>(j,i)]
                                        ) * 255);
      }

      // The last step, like Gaussian smoothing
      CLAHE_GO.at<uchar>(j,i) = CLAHE_GO.at<uchar>(j,i) + (CLAHE_GO.at<uchar>(j,i) << 8) + (CLAHE_GO.at<uchar>(j,i) << 16);
    }
  }

  return CLAHE_GO;
}

cv::Mat claheGO_test (cv::Mat src, int step = 8) {
  int block        = step;
  int width        = src.cols;
  int height       = src.rows;
  int width_block  = width / block;
  int height_block = height / block;

  float **C2 = block_f_test (src, block, width_block, height_block);
  cv::Mat CLAHE_GO = ctpv_test (src, block, width, height, width_block, height_block, C2);

  return CLAHE_GO;
}
//////////////////////////////////////////////////////


/////////////////////// TESTS ////////////////////////
TEST (cal_hist_func, test2) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 8;
  int start_y = 0;
  int end_y   = 8;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test3) {
  cv::Mat src = cv::imread ("../test_images/sand.jpg",0);
  int start_x = 1000;
  int end_x   = 1200;
  int start_y = 567;
  int end_y   = 648;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test4) {
  cv::Mat src = cv::imread ("../test_images/sand.jpg",0);
  int start_x = 600;
  int end_x   = 800;
  int start_y = 81;
  int end_y   = 162;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test5) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 8;
  int start_y = 0;
  int end_y   = 8;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test6) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 8;
  int end_x   = 64;
  int start_y = 0;
  int end_y   = 8;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test7) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 8;
  int start_y = 8;
  int end_y   = 64;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (cal_hist_func, test8) {
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 8;
  int start_y = 0;
  int end_y   = 8;

  int *real_result = cal_hist (src, start_x, end_x, start_y, end_y);
  int *exp_result  = cal_hist_test (src, start_x, end_x, start_y, end_y);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (real_result[i], exp_result[i]);
}

TEST (steal_func, test1) {
//  int *tmp2;
//
//  tmp2 = new int [256];
//
//  srand (time(NULL));
//
//  for (int i = 0; i < 256; i++)
//    tmp2[i] = (uint8_t)rand();
  
  cv::Mat src = cv::imread ("../test_images/1.jpg",0);
  int start_x = 0;
  int end_x   = 93;
  int start_y = 0;
  int end_y   = 70;

  int *tmp2_dummy = cal_hist (src, start_x, end_x, start_y, end_y);
  
  int width_block  = 93;
  int height_block = 70;
  
  auto real_result = steal (tmp2_dummy, width_block, height_block); 
  int *tmp2_real   = std::get<0>(real_result);
  int steal_real   = std::get<1>(real_result);

  tmp2_dummy = cal_hist (src, start_x, end_x, start_y, end_y);

  auto exp_result = steal_test (tmp2_dummy, width_block, height_block);
  int *tmp2_exp   = std::get<0>(exp_result);
  int steal_exp   = std::get<1>(exp_result);

  ASSERT_EQ (steal_real, steal_exp);
  for (int i = 0; i < 256; i++)
    ASSERT_EQ (tmp2_real[i], tmp2_exp[i]);
}

TEST (steal_func, test2) {
  cv::Mat src = cv::imread ("../test_images/lena.bmp",0);
  int start_x = 0;
  int end_x   = 64;
  int start_y = 0;
  int end_y   = 64;

  int *tmp2_dummy = cal_hist (src, start_x, end_x, start_y, end_y);

  int width_block  = 64;
  int height_block = 64;

  auto real_result = steal (tmp2_dummy, width_block, height_block);
  int *tmp2_real   = std::get<0>(real_result);
  int steal_real   = std::get<1>(real_result);

  tmp2_dummy = cal_hist (src, start_x, end_x, start_y, end_y);

  auto exp_result = steal_test (tmp2_dummy, width_block, height_block);
  int *tmp2_exp   = std::get<0>(exp_result);
  int steal_exp   = std::get<1>(exp_result);

  ASSERT_EQ (steal_real, steal_exp);
  for (int i = 0; i < 256; i++)
    ASSERT_EQ (tmp2_real[i], tmp2_exp[i]);
}

TEST (cd_hist_func, test1) {
  cv::Mat src = cv::imread ("../test_images/lena.bmp",0);
  int start_x = 0;
  int end_x   = 64;
  int start_y = 0;
  int end_y   = 64;

  int *tmp2_dummy = cal_hist (src, start_x, end_x, start_y, end_y);

  int width_block  = 64;
  int height_block = 64;
  int total        = width_block * height_block;

  float *C2_real = cd_hist (tmp2_dummy, total);
  float *C2_exp  = cd_hist_test (tmp2_dummy, total);

  for (int i = 0; i < 256; i++)
    ASSERT_EQ (C2_real[i], C2_exp[i]);
}

TEST (block_f_func, test1) {
  cv::Mat src = cv::imread ("../test_images/lena.bmp",0);
  int block        = 8;
  int width        = src.cols;
  int height       = src.rows;
  int width_block  = width / block;
  int height_block = height / block;

  float **C2_real = block_f (src, block, width_block, height_block);
  float **C2_exp  = block_f_test (src, block, width_block, height_block);

  for (int i = 0; i < 8*8; i++)
    for (int j = 0; j < 256; j++)
      ASSERT_EQ (C2_real[i][j], C2_exp[i][j]);
}

TEST (ctpv_func, test1) {
  cv::Mat src = cv::imread ("../test_images/lena.bmp",0);
  int block        = 8;
  int width        = src.cols;
  int height       = src.rows;
  int width_block  = width / block;
  int height_block = height / block;

  float **C2 = block_f (src, block, width_block, height_block);
  cv::Mat CLAHE_GO_real = ctpv (src, block, width, height, width_block, height_block, C2);
  cv::Mat CLAHE_GO_exp  = ctpv_test (src, block, width, height, width_block, height_block, C2);

  for (int i = 0; i < width; i++)
    for (int j = 0; j < height; j++)
      ASSERT_EQ (CLAHE_GO_real.at<uchar>(i,j), CLAHE_GO_exp.at<uchar>(i,j));
}

TEST (claheGO_func, test1) {
  cv::Mat src = cv::imread ("../test_images/lena.bmp",0);
  int block  = 8;
  int width  = src.cols;
  int height = src.rows;

  cv::Mat CLAHE_GO_real = claheGO (src, block);
  cv::Mat CLAHE_GO_exp  = claheGO_test (src, block);

  for (int i = 0; i < width; i++)
    for (int j = 0; j < height; j++)
      ASSERT_EQ (CLAHE_GO_real.at<uchar>(i,j), CLAHE_GO_exp.at<uchar>(i,j));
}
//////////////////////////////////////////////////////
