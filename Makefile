CPP_FILES=$(shell find src/ -name "*.cpp")
OBJ_FILES=$(CPP_FILES:.cpp=.o)
EXE=clahe
CXXFLAGS=--std=c++11 -O3 -g -Wall `pkg-config --cflags --libs opencv`
TEST_DIR=test_images
TEST_IMAGES=$(shell find $(TEST_DIR)/* | awk -F "/" '{print $$2}')
RESULTS_DIR=result_images
PDFS_DIR=result_pdfs

all: $(EXE) run create_pdf open_pdf


$(EXE): $(OBJ_FILES)
	g++ $^ $(CXXFLAGS) -o $@


%.o: %.cpp
	g++ $< -MMD -c $(CXXFLAGS) -o $@


run:
	$(shell mkdir -p $(RESULTS_DIR))
	for file in $(TEST_IMAGES) ; do \
		./$(EXE) $(TEST_DIR)/$${file} $(RESULTS_DIR)/$${file} ; \
	done


create_pdf:
	$(shell mkdir -p $(PDFS_DIR))
	for file in $(TEST_IMAGES) ; do \
		convert $(TEST_DIR)/$${file} $(RESULTS_DIR)/$${file} $(PDFS_DIR)/$${file}.pdf ; \
	done

open_pdf:
	for file in $(TEST_IMAGES) ; do \
		xdg-open $(PDFS_DIR)/$${file}.pdf ; \
	done

.PHONY: clean
clean:
	rm -rf $(EXE) $(RESULTS_DIR) $(PDFS_DIR)
	find -name "*.o" | xargs rm -f
	find -name "*.d" | xargs rm -f
