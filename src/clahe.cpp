/////////////////////////////////////////////////////////////////////
// Description: This code uses CLAHE enhancement (Contrast limited
//              adaptive histogram equalization) to improve the
//              visibility level of foggy images, using opencv.
// 
// Usage      : ./clahe <path_to_input_image> <path_to_output_image>
/////////////////////////////////////////////////////////////////////

#include "claheGO.hpp"

int main (int argc, char** argv) {
    // Read the grayscale image
    cv::Mat src = cv::imread (argv[1],0);
    cv::Mat dst = src.clone();
    cv::Mat CLAHE_GO;

    CLAHE_GO = claheGO (src, 8);
 
    // The results show 
    //imshow("original image", src);
    //imshow("GOCLAHE", CLAHE_GO);
    //cv::waitKey();

    // Save the results
    std::vector<int> compression_params;
    compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

    bool result = false;
    try {
      result = imwrite (argv[2], CLAHE_GO, compression_params);
    }
    catch (const cv::Exception& ex) {
      fprintf (stderr, "clahe: Exception converting image to PNG format: %s\n", ex.what());
    }

    if (result) {
      printf ("clahe: Saved PNG file %s\n", argv[2]);
      return 0;
    } else {
      printf ("clahe: ERROR - Can't save PNG file %s\n", argv[2]);
      return 1;
   }
}
