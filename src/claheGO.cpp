#include "claheGO.hpp"

/**
 * @brief Calculate local diagram for an image block
 * @param [ in] src - Input picture pixel values
 * @param [ in] start_x - Starting point for X axis of block
 * @param [ in] end_x - End point for X axis of block
 * @param [ in] start_y - Starting point for Y axis of block
 * @param [ in] end_y - End point for Y axis of block
 * @param [out] *tmp2 - Returned histogram for image block
 */
int *cal_hist (cv::Mat src, int start_x, int end_x, int start_y, int end_y) {
  int *tmp2;
  
  tmp2 = new int [256];

  for (int i = 0; i < 256; i++)
    tmp2[i] = 0;

  for (int i = start_x; i < end_x; i++) {
    for (int j = start_y; j < end_y; j++) {
      int index = src.at<uchar>(j,i);
      tmp2[index]++;
    }
  }

  return reinterpret_cast<int *>(tmp2);
}

/**
 * @brief Normalization of histogram values using average and a certain gain factor
 * @param [ in] *tmp - Block histogram values
 * @param [ in] width_block - Width size of processed block
 * @param [ in] height_block - Height size of processed block
 * @param [out] *tmp2 - Normalized histogram
 * @param [out] steal - Surplus taken out by normalization
 */
std::tuple<int *, int> steal (int *tmp2, int width_block, int height_block) {
  int average = width_block * height_block / 255;
  int LIMIT = 40 * average;
  int steal = 0;

  for (int k = 0; k < 256; k++) {
    if (tmp2[k] > LIMIT) {
      steal  += tmp2[k] - LIMIT;
      tmp2[k] = LIMIT;
    }
  }

  return std::tuple<int *, int>{tmp2, steal};
} 

/**
 * @brief Calculate the cumulative distribution histogram
 * @param [ in] *tmp2 - Input histogram after normalization 
 * @param [ in] total - Cumulativ ratio
 * @param [out] C2 -  Cumulativ distribution histogram
 */
float *cd_hist (int *tmp2, int total) {
  float *C2;

  C2 = new float [256];

  for (int k = 0; k < 256; k++) {
    if (k == 0)
      C2[k] = 1.0f * tmp2[k] / total;
    else
      C2[k] = C2[k-1] + 1.0f * tmp2[k] / total;
  }

  return reinterpret_cast<float *>(C2);
}

/**
 *  @brief block_f calculates the output image after histogram equalization 
 *  @param [ in] src   - Input matrix
 *  @param [ in] block - number of blocks per axis
 *  @param [ in] width_block  - width number of pixels for each block
 *  @param [ in] height_block - height number of pixels for each block
 *  @param [out] C2 - result image after histogram equalization
 */
float **block_f (cv::Mat src, int block, int width_block, int height_block) {
  float **C2;
  int *tmp2;
  int total = width_block * height_block;
  
  C2 = new float *[8*8];
  for (int i = 0; i < 8*8; i++)
    C2[i] = new float[256];

  for (int i = 0; i < block; i++) {
    for (int j = 0; j < block; j++) {
      int start_x = i * width_block;
      int end_x   = start_x + width_block;
      int start_y = j * height_block;
      int end_y   = start_y + height_block;
      int num     = i + block * j;

      // Traverse the small block, calculate the histogram
      tmp2 = cal_hist (src, start_x, end_x, start_y, end_y);

      // Crop and increase operations, that is, the cl part of the clahe
      // The parameters here correspond to "Gem" above fCliplimit = 4 , uiNrBins = 255
      auto data = steal (tmp2, width_block, height_block);
      tmp2      = std::get<0>(data);
      int steal = std::get<1>(data);

      int bonus = steal / 256;

      // hand out the steals averagely
      for (int k = 0; k < 256; k++) {
        tmp2[k] += bonus;
      }
  
      C2[num] = cd_hist (tmp2, total);
    }
  }

  return reinterpret_cast<float **>(C2);
}

/**
 *  @brief Calculate final pixel values based on the location in the matrix using linear interpolation
 *  @param [ in] src   - Input matrix
 *  @param [ in] block - number of blocks per axis
 *  @param [ in] width - picture pixel width
 *  @param [ in] height - picture pixel height
 *  @param [ in] width_block  - width number of pixels for each block
 *  @param [ in] height_block - height number of pixels for each block
 *  @param [ in] **C2 - result picture after histogram equalization
 *  @param [out] CLAHE_GO - return final picture
 */
cv::Mat ctpv (cv::Mat src, int block, int width, int height, int width_block, int height_block, float **C2) {
  cv::Mat CLAHE_GO = src.clone();

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // four coners  
      if (i <= width_block / 2 && j <= height_block / 2) {
        int num = 0;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i <= width_block / 2 && j >= ((block - 1) * height_block + height_block / 2)) {
        int num = block * (block - 1);
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2) && j <= height_block / 2) {
        int num = block - 1;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2) && j >= ((block - 1) * height_block + height_block / 2)) {
        int num = block * block - 1;
        CLAHE_GO.at<uchar>(j,i) = (int)(C2[num][CLAHE_GO.at<uchar>(j,i)] * 255);
      }
      // four edges except coners  
      else if (i <= width_block / 2) {
        //linear interpolation  
        int num_i = 0;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2)) {
        // linear interpolation  
        int num_i = block - 1;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j <= height_block / 2) {
        // linear interpolation  
        int num_i = (i - width_block / 2) / width_block;
        int num_j = 0;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j >= ((block - 1) * height_block + height_block / 2)) {
        // linear interpolation  
        int num_i = (i - width_block / 2) / width_block;
        int num_j = block - 1;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      }
      // four edges except coners  
      else if (i <= width_block / 2) {
        //linear interpolation  
        int num_i = 0;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (i >= ((block - 1) * width_block + width_block / 2)) {
        // linear interpolation  
        int num_i = block - 1;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + block;
        float p   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j <= height_block / 2) {
        // linear interpolation  
        int num_i = (i - width_block / 2) / width_block;
        int num_j = 0;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      } else if (j >= ((block - 1) * height_block + height_block / 2)) {
        // linear interpolation  
        int num_i = (i - width_block / 2) / width_block;
        int num_j = block - 1;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        float p   = (i - (num_i * width_block + width_block / 2)) / (1.0f * width_block);
        float q   = 1 - p;
        CLAHE_GO.at<uchar>(j,i) = (int)((q * C2[num1][CLAHE_GO.at<uchar>(j,i)] + p * C2[num2][CLAHE_GO.at<uchar>(j,i)]) * 255);
      }
      // bilinear interpolation
      else {
        int num_i = (i - width_block / 2) / width_block;
        int num_j = (j - height_block / 2) / height_block;
        int num1  = num_j * block + num_i;
        int num2  = num1 + 1;
        int num3  = num1 + block;
        int num4  = num2 + block;
        float u   = (i - (num_i * width_block  + width_block  / 2)) / (1.0f * width_block);
        float v   = (j - (num_j * height_block + height_block / 2)) / (1.0f * height_block);
        CLAHE_GO.at<uchar>(j,i) = (int)((   u  *    v  * C2[num4][CLAHE_GO.at<uchar>(j,i)] +
                                         (1-u) * (1-v) * C2[num1][CLAHE_GO.at<uchar>(j,i)] +
                                            u  * (1-v) * C2[num2][CLAHE_GO.at<uchar>(j,i)] +
                                         (1-u) *    v  * C2[num3][CLAHE_GO.at<uchar>(j,i)]
                                        ) * 255);
      }

      // The last step, like Gaussian smoothing
      CLAHE_GO.at<uchar>(j,i) = CLAHE_GO.at<uchar>(j,i) + (CLAHE_GO.at<uchar>(j,i) << 8) + (CLAHE_GO.at<uchar>(j,i) << 16);
    }
  }

  return CLAHE_GO;
}
/**
 *  @brief Wrapper for block_f and ctpv functions
 *  @param [ in] src   - Input matrix
 *  @param [ in] block - number of blocks per axis
 *  @param [out] CLAHE_GO - return final picture
 */
cv::Mat claheGO (cv::Mat src, int step = 8) {
  int block        = step;          // pblock
  int width        = src.cols;
  int height       = src.rows;
  int width_block  = width / block; // the length and width of each small lattice
  int height_block = height / block;

  // This is used for saving the initial matrix for future stimuli
  //std::cout << "result = " << std::endl << " "  << src << std::endl << std::endl;

  // Block 
  float **C2 = block_f (src, block, width_block, height_block);

  // This is used for saving the cumulative distribution histogram for future comparison
  //std::cout << "cumulative distributon_histogram =" << std::endl;
  //for (int i = 0; i < 8*8; i++) {
  //  for (int j = 0; j < 256; j++)
  //    std::cout << C2[i][j] << " ";
  //  std::cout << std::endl;
  //}

  // Calculate the transformed pixel value  
  // According to the location of the pixel, choose a different calculation method  
  cv::Mat CLAHE_GO = ctpv (src, block, width, height, width_block, height_block, C2);

  // This is used for saving the computed matrix for future comparison
  //std::cout << "result = " << std::endl << " "  << CLAHE_GO << std::endl << std::endl;

  return CLAHE_GO;
}
