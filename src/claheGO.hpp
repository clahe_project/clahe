#pragma once

#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>

int *cal_hist (cv::Mat src, int start_x, int end_x, int start_y, int end_y);
std::tuple<int *, int> steal (int *tmp2, int width_block, int height_block);
float *cd_hist (int *tmp2, int total);
float **block_f (cv::Mat src, int block, int width_block, int height_block);
cv::Mat ctpv (cv::Mat src, int block, int width, int height, int width_block, int height_block, float **C2);
cv::Mat claheGO(cv::Mat src, int step);
